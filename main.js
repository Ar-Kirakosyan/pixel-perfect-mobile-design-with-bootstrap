$(document).on('click', '#search-key-word input', function(){
	$(this).parent().find('i').addClass('show')
	$('.list-wrap').attr('style', 'display: none');
	$('.search-result-wrap').attr('style', 'display: block');
})


$(document).on('click', '.search-result-wrap' , function(){
	$('#search-key-word i').removeClass('show')
	$('.list-wrap').attr('style', 'display: block');
	$('.search-result-wrap').attr('style', 'display: none');
})

$(document).on('click', '#search-key-word i', function(){
	$(this).removeClass('show')
	$('.list-wrap').attr('style', 'display: block');
	$('.search-result-wrap').attr('style', 'display: none');
})

$(document).on('click', '.header .fa-ellipsis-v', function(){
	$('.pop-up-menu').toggle();
	$('.overlay').toggle();
})

$(document).on('click', '.pop-up-menu .menu', function(){
	$('.pop-up-menu').attr('style', 'display: none')
	$('.overlay').attr('style', 'display: none')
})

$(document).on('click', '.overlay', function(){
	$('.pop-up-menu').attr('style', 'display: none')
	$('.overlay').attr('style', 'display: none')
})



var items = [
	{
		label: 'Route',
		type: 'Symptom'
	},
	{
		label: 'Carcinoid',
		type: 'Diagnosis'
	},
	{
		label: 'Cough',
		type: 'Symptom'
	},
	{
		label: 'Angel',
		type: 'Symptom'
	},
	{
		label: 'Low Potassium',
		type: 'Diagnose'
	}
]

$(document).ready(function(){
	
	$(items).each(function(){
		str = '<div class="list-item">'
		str += '<span>' + this.label + '</span>'
		str += '<i class="fa fa-chevron-right"></i></div>'
		$('.list-wrap').append(str)
	})	
})

function autoComplete(input, arr){
	let currentKey;
	input.on('input', function(e){ // Display matching items
		let val = event.target.value;
		currentKey = -1;
		$('.search-result').empty();
		for(let idx=0; idx<arr.length; idx++){
			if(arr[idx].label.substr(0, val.length).toUpperCase() == val.toUpperCase()){
				if(arr[idx].type === 'Diagnosis')
					$('.search-result').append('<div class="search-result-item"><div class="label"><span>'+arr[idx].label.substr(0, val.length)+'</span>'+arr[idx].label.substr(val.length)+'</div>' + '<div class="type green">' + arr[idx].type + '</div></div>');
				else
					$('.search-result').append('<div class="search-result-item"><div class="label"><span>'+arr[idx].label.substr(0, val.length)+'</span>'+arr[idx].label.substr(val.length)+'</div>' + '<div class="type blue">' + arr[idx].type + '</div></div>');
		  	}
		}
	});
  
	$(document).on('click', '.search-result-item', function(e){
		let val = $(this).find('.label').text();
		$('#search-key-word input').val(val);
		$('.search-result').empty();
	});
  
}

autoComplete($('#search-key-word input'), items);
